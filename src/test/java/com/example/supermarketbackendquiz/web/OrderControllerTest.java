package com.example.supermarketbackendquiz.web;

import com.example.supermarketbackendquiz.domain.Commodity;
import com.example.supermarketbackendquiz.domain.CommodityRepository;
import com.example.supermarketbackendquiz.domain.Order;
import com.example.supermarketbackendquiz.domain.OrderRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class OrderControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private CommodityRepository commodityRepository;

    @Test
    void shouldReturnCommodities() throws Exception {
        initCommoditiesData();
        mockMvc.perform(get("/api/orders"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].commodity.name").value("苹果"))
                .andExpect(jsonPath("$[0].commodity.price").value(5.00))
                .andExpect(jsonPath("$[0].commodity.unit").value("斤"))
                .andExpect(jsonPath("$[0].commodity.image").value("localhost"));
    }
    @Test
    void deleteOrderByCommodityId() throws Exception {
        initCommoditiesData();
        mockMvc.perform(delete("/api/orders/commodities/1"))
                .andExpect(status().is(204));
    }

    @Test
    void addOrder() throws Exception {
        initCommoditiesData();
        mockMvc.perform(post("/api/orders/commodities/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is(201));
    }


    //TODO: it is not init commodity
    private void initCommoditiesData() {
        Commodity saveCommodity = commodityRepository.saveAndFlush(new Commodity("苹果", 5.00, "斤", "localhost"));
        Order order = new Order(saveCommodity);
        orderRepository.saveAndFlush(order);
    }
}