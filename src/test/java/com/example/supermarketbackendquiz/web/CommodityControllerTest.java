package com.example.supermarketbackendquiz.web;

import com.example.supermarketbackendquiz.domain.Commodity;
import com.example.supermarketbackendquiz.domain.CommodityRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class CommodityControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private CommodityRepository commodityRepository;

    @Test
    void shouldReturnCommodities() throws Exception {
        initCommoditiesData();
        mockMvc.perform(get("/api/commodities"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].name").value("苹果"))
                .andExpect(jsonPath("$[0].price").value(5.00))
                .andExpect(jsonPath("$[0].unit").value("斤"))
                .andExpect(jsonPath("$[0].image").value("localhost"));
    }
    @Test
    void addCommodity() throws Exception {
        mockMvc.perform(post("/api/commodities")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new Commodity("苹果", 5.00, "斤", "localhost"))))
                .andExpect(status().is(201));
    }


    private void initCommoditiesData() {
        // TODO: image data does not express the meaning
        Commodity commodity = new Commodity("苹果", 5.00, "斤", "localhost");
        commodityRepository.save(commodity);
    }
}