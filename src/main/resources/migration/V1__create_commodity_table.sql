CREATE TABLE IF NOT EXISTS commodity
(
    id    BIGINT AUTO_INCREMENT,
    name  VARCHAR(128) NOT NULL,
    price Decimal      NOT NULL,
    unit  varchar(20)  not null,
    image varchar(225) not null,
    PRIMARY KEY (id)
);