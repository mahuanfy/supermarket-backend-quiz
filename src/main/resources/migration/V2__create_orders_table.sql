CREATE TABLE orders
(
    id          BIGINT AUTO_INCREMENT,
    commodity_id bigint not null,
    PRIMARY KEY (id)
);

ALTER TABLE orders
    ADD CONSTRAINT `FK_order_commodity` FOREIGN KEY (commodity_id) REFERENCES commodity (`id`);

