package com.example.supermarketbackendquiz.web;

import com.example.supermarketbackendquiz.domain.Commodity;
import com.example.supermarketbackendquiz.domain.CommodityRepository;
import com.example.supermarketbackendquiz.exception.ExistElementException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/commodities")
@CrossOrigin(origins = "*")
public class CommodityController {
    private final CommodityRepository repository;

    public CommodityController(CommodityRepository repository) {
        this.repository = repository;
    }

    @GetMapping
    public ResponseEntity getCommodities() {
        List<Commodity> commodities = repository.findAll();
        return ResponseEntity.ok(commodities);
    }

    @PostMapping
    public ResponseEntity addCommodity(@RequestBody @Valid Commodity commodity) {
        Optional<Commodity> findCommodity = repository.findByName(commodity.getName());
        if(findCommodity.isPresent()){
            throw new ExistElementException("商品名称已存在，请输入新的商品名称");
        }

        repository.save(commodity);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
