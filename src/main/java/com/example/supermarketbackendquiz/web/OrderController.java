package com.example.supermarketbackendquiz.web;

import com.example.supermarketbackendquiz.domain.Commodity;
import com.example.supermarketbackendquiz.domain.CommodityRepository;
import com.example.supermarketbackendquiz.domain.Order;
import com.example.supermarketbackendquiz.domain.OrderRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@RestController
@RequestMapping("/api/orders")
@CrossOrigin(origins = "*")
public class OrderController {
    private final OrderRepository repository;
    private final CommodityRepository commodityRepository;

    public OrderController(OrderRepository repository, CommodityRepository commodityRepository) {
        this.repository = repository;
        this.commodityRepository = commodityRepository;
    }

    //TODO: it is not get commodity...
    @GetMapping
    public ResponseEntity getCommodities() {
        List<Order> orders = repository.findAll();
        return ResponseEntity.ok(orders);
    }

    //todo: not restful
    @DeleteMapping("/commodities/{commodityId}")
    public ResponseEntity deleteOrderByCommodityId(@PathVariable Long commodityId) {
        List<Order> orders = repository.findByCommodityId(commodityId);
        repository.deleteAll(orders);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    //todo: not restful
    @PostMapping("/commodities/{commodityId}")
    public ResponseEntity AddOrder(@PathVariable Long commodityId) {
        Commodity commodity = commodityRepository.findById(commodityId).orElseThrow(NoSuchElementException::new);
        Order order = new Order(commodity);
        repository.save(order);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
