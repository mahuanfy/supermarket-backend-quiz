package com.example.supermarketbackendquiz.exception;

public class ExistElementException extends RuntimeException {
    public ExistElementException() {
        super();
    }

    public ExistElementException(String message) {
        super(message);
    }
}
