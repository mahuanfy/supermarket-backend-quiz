package com.example.supermarketbackendquiz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SupermarketBackendQuizApplication {

    public static void main(String[] args) {
        SpringApplication.run(SupermarketBackendQuizApplication.class, args);
    }

}
