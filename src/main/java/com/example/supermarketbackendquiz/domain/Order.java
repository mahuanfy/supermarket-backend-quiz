package com.example.supermarketbackendquiz.domain;

import javax.persistence.*;

@Entity(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "commodity_id")
    private Commodity commodity;

    public Order() {
    }

    public Order(Commodity commodity) {
        this.commodity = commodity;
    }

    public Long getId() {
        return id;
    }

    public Commodity getCommodity() {
        return commodity;
    }
}
