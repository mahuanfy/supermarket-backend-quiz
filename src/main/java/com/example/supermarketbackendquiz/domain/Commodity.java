package com.example.supermarketbackendquiz.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@NotNull
public class Commodity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 128)
    private String name;
    @Column(length = 10)
    private Double price;
    @Column(length = 20)
    private String unit;
    // TODO: considered url length ?
    @Column(length = 225)
    private String image;

    public Commodity() {
    }

    public Commodity(String name, Double price, String unit, String image) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.image = image;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImage() {
        return image;
    }
}
